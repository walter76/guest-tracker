import React, { useState } from 'react'
import styled from 'styled-components'

import Configuration from '../../common/configuration'

const Page = styled.div``

const Title = styled.h2``

const FormFieldsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 5px;
`

const ButtonRow = styled.div`
  display: flex;
  justify-content: space-between;
`

const Registration = () => {
  const [lastname, setLastname] = useState('')
  const [firstname, setFirstname] = useState('')
  const [mobile, setMobile] = useState('')
  const [tableNumber, setTableNumber] = useState('')
  const [date, setDate] = useState('')
  const [from, setFrom] = useState('')
  const [to, setTo] = useState('')

  const onSubmit = (event) => {
    console.log({
      lastname,
      firstname,
      mobile,
      tableNumber,
      date,
      from,
      to,
    })

    event.preventDefault()
  }

  const onCancel = (_) => {
    setLastname('')
    setFirstname('')
    setMobile('')
    setTableNumber('')
    setDate('')
    setFrom('')
    setTo('')
  }

  return (
    <Page>
      <Title>Welcome to {Configuration.name}!</Title>
      <form onSubmit={onSubmit}>
        <FormFieldsContainer>
          <input
            type='text'
            placeholder='Lastname'
            value={lastname}
            onChange={(e) => setLastname(e.target.value)}
            autoFocus
          />
          <input
            type='text'
            placeholder='Firstname'
            value={firstname}
            onChange={(e) => setFirstname(e.target.value)}
          />
          <input
            type='text'
            placeholder='Mobile'
            value={mobile}
            onChange={(e) => setMobile(e.target.value)}
          />
          <hr />
          <input
            type='text'
            placeholder='Table number'
            value={tableNumber}
            onChange={(e) => setTableNumber(e.target.value)}
          />
          <input
            type='text'
            placeholder='Date'
            value={date}
            onChange={(e) => setDate(e.target.value)}
          />
          <input
            type='text'
            placeholder='From'
            value={from}
            onChange={(e) => setFrom(e.target.value)}
          />
          <input
            type='text'
            placeholder='To'
            value={to}
            onChange={(e) => setTo(e.target.value)}
          />
          <hr />
          <ButtonRow>
            <input type='submit' value='Register' />
            <input type='button' value='Cancel' onClick={onCancel} />
          </ButtonRow>
        </FormFieldsContainer>
      </form>
    </Page>
  )
}

export default Registration
