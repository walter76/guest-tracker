import React from 'react'
import styled from 'styled-components'

import GlobalStyle from './globalStyle'
import Registration from '../features/registration/Registration'

const AppWrapper = styled.div`
  background-color: #fafafa;
  margin: 5px auto;
  padding: 2px 10px 2px 10px;
  max-width: 75%;
`

const AppTitle = styled.h1``

const App = () => (
  <>
    <GlobalStyle />
    <AppWrapper>
      <AppTitle>guest-tracker-app</AppTitle>
      <Registration />
    </AppWrapper>
  </>
)

export default App
