var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapPost("api/registrations", (GuestRegistration registration) =>
{
    app.Logger.LogInformation($"Registering {registration.Lastname}, {registration.Firstname}");
});

app.Run();

record GuestRegistration(string Lastname, string Firstname, string Phone, int TableNumber, string Date, string From, string To)
{
}
