import { red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

// https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=008e00&secondary.color=f9a000

export default createMuiTheme({
  palette: {
    primary: {
      light: '#50bf3e',
      main: '#008e00',
      dark: '#005f00',
      contrastText: '#000'
    },
    secondary: {
      light: '#ffd148',
      main: '#f9a000',
      dark: '#c07100',
      contrastText: '#000'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#fff'
    }
  }
})
