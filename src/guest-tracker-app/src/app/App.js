import React from 'react'

import Registration from '../features/registration/Registration'

export default () => (
  <div>
    <div>guest-tracker-app</div>
    <Registration />
  </div>
)
