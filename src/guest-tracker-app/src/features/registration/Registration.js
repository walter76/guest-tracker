import React, { useState } from 'react'

import { makeStyles } from '@material-ui/core/styles'
import {
  Paper,
  Typography,
  TextField,
  Button,
  InputAdornment
} from '@material-ui/core'
import PhoneIcon from '@material-ui/icons/Phone'

import useClientService from '../../common/useClientService'

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
    width: '480px'
  },
  guestRegistration: {
    padding: theme.spacing(2)
  },
  registrationForm: {
    display: 'flex',
    flexDirection: 'column',

    '& > *': {
      marginTop: theme.spacing(2)
    }
  }
}))

const onChange = (f) => (e) => f(e.target.value)

const formatAsISODate = (date) => {
  const isoDateTimeString = date.toISOString()
  return isoDateTimeString.substring(0, isoDateTimeString.indexOf('T'))
}

const formatAsISOTime = (date) => {
  const isoDateTimeString = date.toISOString()
  return isoDateTimeString.substring(
    isoDateTimeString.indexOf('T') + 1,
    isoDateTimeString.indexOf('T') + 9
  )
}

export default () => {
  const clientService = useClientService()

  const restaurantName = 'A great place to eat!'
  const [lastname, setLastname] = useState('')
  const [firstname, setFirstname] = useState('')
  const [phone, setPhone] = useState('')
  const [tableNumber, setTableNumber] = useState('')

  const now = new Date()
  const isoTimeString = formatAsISOTime(now)
  const [date, setDate] = useState(formatAsISODate(now))
  const [from, setFrom] = useState(isoTimeString)
  const [to, setTo] = useState(isoTimeString)

  const onSubmit = (event) => {
    clientService.registerGuest({
      firstname,
      lastname,
      phone,
      tableNumber,
      date,
      from,
      to
    })

    event.preventDefault()
  }

  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Paper>
        <div className={classes.guestRegistration}>
          <Typography variant="h5" noWrap>
            {restaurantName} - Guest registration
          </Typography>
          <form onSubmit={onSubmit}>
            <div className={classes.registrationForm}>
              <TextField
                fullWidth
                required
                autoFocus
                name="lastname"
                placeholder="Lastname"
                label="Lastname"
                size="small"
                value={lastname}
                onChange={onChange(setLastname)}
              />
              <TextField
                fullWidth
                required
                name="firstname"
                placeholder="Firstname"
                label="Firstname"
                size="small"
                value={firstname}
                onChange={onChange(setFirstname)}
              />
              <TextField
                fullWidth
                required
                name="phone"
                placeholder="Phone"
                label="Phone"
                size="small"
                value={phone}
                onChange={onChange(setPhone)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PhoneIcon />
                    </InputAdornment>
                  )
                }}
              />
              <TextField
                fullWidth
                required
                name="tableNumber"
                placeholder="Table Number"
                label="Table Number"
                size="small"
                value={tableNumber}
                onChange={onChange(setTableNumber)}
              />
              <TextField
                fullWidth
                required
                type="date"
                name="date"
                label="Date of Visit"
                size="small"
                value={date}
                onChange={onChange(setDate)}
              />
              <TextField
                fullWidth
                required
                type="time"
                name="from"
                label="From"
                size="small"
                value={from}
                onChange={onChange(setFrom)}
              />
              <TextField
                fullWidth
                required
                type="time"
                name="to"
                label="To"
                size="small"
                value={to}
                onChange={onChange(setTo)}
              />

              <Button type="submit" color="inherit">
                Register
              </Button>
            </div>
          </form>
        </div>
      </Paper>
    </div>
  )
}
