export default {
  registerGuest: async (registerInformation) => {
    const {
      firstname,
      lastname,
      phone,
      tableNumber,
      date,
      from,
      to
    } = registerInformation
    console.log(
      `Registering guest ${firstname}, ${lastname}, ${phone}, ${tableNumber}, ${date}, ${from}, ${to}`
    )
  }
}
